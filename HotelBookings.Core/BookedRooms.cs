﻿using System;
using System.Collections.Concurrent;

namespace HotelBookings.Core
{
    internal class BookedRooms
    {
        internal readonly ConcurrentDictionary<int, BookedDates> Rooms;

        public BookedRooms()
        {
            Rooms = new ConcurrentDictionary<int, BookedDates> ();
        }

        internal void AddOrAmend(int room, DateTime date)
        {
            var roomExists = Rooms.TryGetValue(room, out var bookedDates);
            var bookingSuccessful = false;

            if (!roomExists)
            {
                bookingSuccessful = InitializeRoomBooking(room, date);
            }

            if (!bookingSuccessful && bookedDates.IsEmpty(date))
            {
                bookingSuccessful = UpdateBookedDates(room, date, bookedDates);
            }

            if (!bookingSuccessful)
            {
                throw new InvalidOperationException($"The room {room} is already booked on the {date}!");
            }
        }

        private bool UpdateBookedDates(int room, DateTime date, BookedDates bookedDates)
        {
            var initialBookedDates = bookedDates.GetCount();
            bookedDates.AddDate(date);
            ConcurrentDictionaryHelper.UpdateBookedDates(Rooms, room, bookedDates);
            return SuccessfullyUpdated(initialBookedDates, room);
        }

        private bool SuccessfullyUpdated(int initialBookedDates, int room)
        {
            Rooms.TryGetValue(room, out var newBookedDates);
            return newBookedDates.GetCount() == initialBookedDates + 1;
        }

        private bool InitializeRoomBooking(int room, DateTime date)
        {
            var newBookedDate = new BookedDates(date);
            return Rooms.TryAdd(room, newBookedDate);
        }
    }
}
