﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace HotelBookings.Core
{
    /// <summary>
    /// * The booking system may be used by a number of hotel staff at once, so your implementation must be thread-safe. 
    /// * There is no need to write an interactive harness or command-line interface. Simply call your code from a main method or unit test to demonstrate it works. 
    /// * There is no need to implement a persistent store. 
    /// </summary>
    internal class Booking
    {
        /// <summary>
        /// Guests are identified by their surname which, for the purposes of this exercise, can be considered unique. 
        /// </summary>
        internal readonly ConcurrentBag<string> Guests;

        /// <summary>
        /// Rooms are identified by a unique number taken from an arbitrary, potentially nonsequential set of numbers
        /// </summary>
        internal readonly BookedRooms BookedRooms;

        public Booking()
        {
            Guests = new ConcurrentBag<string>();
            BookedRooms = new BookedRooms();
        }

        internal IEnumerable<int> GetAvailableRooms(DateTime date)
        {
            foreach (var (room, bookedDates) in BookedRooms.Rooms)
            {
                if (bookedDates.Contains(date))
                {
                    yield return room;
                }
            }
        }

        internal void AddBooking(string guest, int room, DateTime date)
        {
            BookedRooms.AddOrAmend(room, date);
            Guests.Add(guest);
        }

        internal bool IsRoomAvailable(int room, DateTime date)
        {
            var roomExists = BookedRooms.Rooms.TryGetValue(room, out var bookedDates);
            if (!roomExists)
            {
                return true;
            }
            return bookedDates.IsEmpty(date);
        }
    }
}
