﻿using HotelBookings.Interfaces;
using System;
using System.Collections.Generic;

namespace HotelBookings.Core
{

    public class BookingManager : IBookingManager
    {
        private readonly Booking BookingSystem;

        public BookingManager()
        {
            BookingSystem = new Booking();
        }

        public void AddBooking(string guest, int room, DateTime date)
        {
            BookingSystem.AddBooking(guest, room, date);
        }

        public bool IsRoomAvailable(int room, DateTime date)
        {
            return BookingSystem.IsRoomAvailable(room, date);
        }

        public IEnumerable<int> GetAvailableRooms(DateTime date)
        {
            return BookingSystem.GetAvailableRooms(date);
        }
    }
}
