﻿using System.Collections.Concurrent;

namespace HotelBookings.Core
{
    public static class ConcurrentDictionaryHelper
    {
        public static void UpdateBookedDates<K, V>(this ConcurrentDictionary<K, V> dictionary, K key, V value)
        {            
            dictionary.AddOrUpdate(key, value, (oldkey, oldvalue) => value);
        }
    }
}
