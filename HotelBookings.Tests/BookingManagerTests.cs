﻿using HotelBookings.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace HotelBookings.Tests
{
    [TestClass]
    public class BookingManagerTests
    {
        private const string TestGuest = "Spock";
        private const int TestRoom = 123;
        private readonly DateTime TestDate = new(2021, 06, 30);

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "The room 101 is already booked on the 28/03/2012 00:00:00!")]
        public void ShouldBeAbleToBehaveAsSpec()
        {
            var sut = new BookingManager();
            var today = new DateTime(2012, 3, 28);

            Assert.IsTrue(sut.IsRoomAvailable(101, today)); 

            sut.AddBooking("Patel", 101, today);

            Assert.IsFalse(sut.IsRoomAvailable(101, today)); 

            sut.AddBooking("Li", 101, today); 
        }

        [TestMethod]
        public void ShouldBeAbleToSeeUnavailableRoom()
        {
            var sut = new BookingManager();

            sut.AddBooking(TestGuest, TestRoom, TestDate);

            var roomAvailable = sut.IsRoomAvailable(TestRoom, TestDate);
            Assert.IsFalse(roomAvailable);
        }

        [TestMethod]
        public void ShouldBeAbleToSeeAvailableRoom()
        {
            var sut = new BookingManager();
            var newDate = new DateTime(2021, 06, 29);

            sut.AddBooking(TestGuest, TestRoom, TestDate);

            var roomAvailable = sut.IsRoomAvailable(TestRoom, newDate);
            Assert.IsTrue(roomAvailable);
        }

        [TestMethod]
        public void ShouldBeAbleToBookSameRoomOnDifferentDay()
        {
            var sut = new BookingManager();
            var newDate = new DateTime(2021, 06, 29);

            sut.AddBooking(TestGuest, TestRoom, TestDate);
            sut.AddBooking(TestGuest, TestRoom, newDate);

            CollectionAssert.Contains(sut.GetAvailableRooms(TestDate).ToList(), TestRoom);
            CollectionAssert.Contains(sut.GetAvailableRooms(newDate).ToList(), TestRoom);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "The room 123 is already booked on the 30/06/2021 00:00:00!")]
        public void ShouldNotBeAbleToBookSameRoomTwiceOnSameDay()
        {
            var sut = new BookingManager();
            var newGuest = "Kirk";

            sut.AddBooking(TestGuest, TestRoom, TestDate);
            sut.AddBooking(newGuest, TestRoom, TestDate);
        }

        [TestMethod]
        public void ShouldGetAvailableRoomOnDemand()
        {
            var sut = new BookingManager();

            sut.AddBooking(TestGuest, TestRoom, TestDate);

            var availableRooms = sut.GetAvailableRooms(TestDate);
            CollectionAssert.Contains(availableRooms.ToList(), TestRoom);
        }

        [TestMethod]
        public void ShouldGetAllAvailableRoomsOnDemand()
        {
            var sut = new BookingManager();
            var room2 = 321;
            var room3 = 456;

            sut.AddBooking(TestGuest, TestRoom, TestDate);
            sut.AddBooking(TestGuest, room2, TestDate);
            sut.AddBooking(TestGuest, room3, TestDate);


            var availableRooms = sut.GetAvailableRooms(TestDate).ToList();
            CollectionAssert.Contains(availableRooms, TestRoom);
            CollectionAssert.Contains(availableRooms, room2);
            CollectionAssert.Contains(availableRooms, room3);
        }

        [TestMethod]
        public void ShouldNotGetAvailableRoomOnDifferentDate()
        {
            var sut = new BookingManager();
            var room2 = 321;
            var newDate = new DateTime(2021, 06, 29);

            sut.AddBooking(TestGuest, TestRoom, TestDate);
            sut.AddBooking(TestGuest, room2, newDate);


            var availableRooms = sut.GetAvailableRooms(TestDate).ToList();
            CollectionAssert.Contains(availableRooms, TestRoom);
            CollectionAssert.DoesNotContain(availableRooms, room2);
        }
    }
}
